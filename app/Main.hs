module Main where

import Brick.Main
import System.Random

import GameUI
import GameMap
import GameState
import DungeonSettings
import DungeonGenerator

main :: IO ()
main = do
  g <- newStdGen
  let (gamemap, g') = randomDungeon defaultDungeonSettings g
  let (gamestate, g'') = randomPlayerStart (newGameStateWithMap gamemap) g'
  defaultMain app $ newUIStateWithGameState gamestate
  putStrLn ""
