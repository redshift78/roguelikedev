{-# Language OverloadedStrings #-}

module GameState
  ( GameState (..)
  , Direction (..)
  , defaultGameState
  , newGameStateWithMap
  , randomPlayerStart
  , performAction
  , movePlayer
  , gameMapWithEntities
  , tileIsSeen
  , tileIsVisible
  ) where

import System.Random

import Entity
import GameMap
import DungeonGenerator

data GameState = GameState
  { player    :: Entity
  , gameMap   :: GameMap
  , gameText  :: [String]
  } deriving (Eq, Show)

defaultGameState :: GameState
defaultGameState = GameState
  { player    = newEntity "player" 10 10 '@' "player"
  , gameMap   = walledGameMap 80 30
  , gameText  = ["Welcome to roguelikedev"]
  }

markPlayerCanSee :: GameState -> GameMap
markPlayerCanSee gs = foldr (\(x,y) m -> setGameTileSeen x y m) gm vtiles
  where gm      = gameMap gs
        p       = player gs
        w       = gameMapWidth gm
        h       = gameMapHeight gm
        vtiles  = filter (canSee gm p 8) [ (x,y) | x <- [0..w-1], y <- [0..h-1] ]

newGameStateWithMap :: GameMap -> GameState
newGameStateWithMap gm = defaultGameState { gameMap = gm }
  where gs  = defaultGameState { gameMap = gm }
        gm' = markPlayerCanSee gs

placePlayer :: GameState -> Int -> Int -> GameState
placePlayer gs x y = gs' { gameMap = gm' }
  where p   = player gs
        gs' = gs { player = p { entityXPos = x, entityYPos = y } }
        gm' = markPlayerCanSee gs'

randomPlayerStart :: RandomGen g => GameState -> g -> (GameState, g)
randomPlayerStart gs g
  | attr == "floor" = (placePlayer gs px py, g'')
  | otherwise       = randomPlayerStart gs g''
  where gm        = gameMap gs
        w         = gameMapWidth gm
        h         = gameMapHeight gm
        (px, g')  = randomR (0, w-1) g
        (py, g'') = randomR (0, h-1) g'
        tile      = getGameTile (px,py) gm
        attr      = tileVisibleAttrName tile

data Direction  = DirectionLeft | DirectionRight | DirectionUp | DirectionDown
                | DirectionUpLeft | DirectionDownLeft | DirectionUpRight | DirectionDownRight
                deriving (Eq, Show)

actionOnEntity :: String -> Entity -> GameState -> GameState
actionOnEntity action e gs = gs { gameText = text' }
  where gm    = gameMap gs
        text' = (action <> entityName e) : gameText gs

-- player performs action in direction
performAction :: Direction -> GameState -> GameState
performAction dir state
  | not (null es) = foldr (actionOnEntity "You kick the ") state es
  | otherwise     = movePlayer dir state
  where p   = player state
        px  = entityXPos p
        py  = entityYPos p
        es  = entitiesAtPos (nx, ny) (gameMap state)
        (nx, ny)  | dir == DirectionLeft      = (px-1, py)
                  | dir == DirectionRight     = (px+1, py)
                  | dir == DirectionUp        = (px, py-1)
                  | dir == DirectionDown      = (px, py+1)
                  | dir == DirectionUpLeft    = (px-1, py-1)
                  | dir == DirectionUpRight   = (px+1, py-1)
                  | dir == DirectionDownLeft  = (px-1, py+1)
                  | dir == DirectionDownRight = (px+1, py+1)
                  | otherwise                 = (px, py)

movePlayer :: Direction -> GameState -> GameState
movePlayer dir state
  | canwalk     = moved { gameMap = gm' }
  | otherwise   = state
  where p       = player state
        x       = entityXPos p
        y       = entityYPos p
        dx      | dir == DirectionLeft      = -1
                | dir == DirectionRight     = 1
                | dir == DirectionUpLeft    = -1
                | dir == DirectionDownLeft  = -1
                | dir == DirectionUpRight   = 1
                | dir == DirectionDownRight = 1
                | otherwise                 = 0
        dy      | dir == DirectionUp        = -1
                | dir == DirectionDown      = 1
                | dir == DirectionUpLeft    = -1
                | dir == DirectionDownLeft  = 1
                | dir == DirectionUpRight   = -1
                | dir == DirectionDownRight = 1
                | otherwise                 = 0
        canwalk = tileWalkable $ getGameTile (x+dx,y+dy) $ gameMapWithEntities state
        moved   = state { player = moveEntity p dx dy }
        gm'     = markPlayerCanSee moved

-- TODO there's some asymmetry here
-- calculate points on line using Bresenham's alg
bresenhamPoints :: (Int, Int) -> (Int, Int) -> [(Int, Int)]
bresenhamPoints (x0, y0) (x1, y1)
  | x1 < x0   = reverse $ bresenhamPoints (x1, y1) (x0, y0)
  | dx >= dy  = [ (x, calcy x) | x <- [x0..x1] ]
  | y0 <= y1  = [ (calcx y, y) | y <- [y0..y1] ]
  | otherwise = reverse [ (calcx y, y) | y <- [sy..by] ]
  where dx      = abs $ x1 - x0
        dy      = abs $ y1 - y0
        by      | y1 >= y0  = y1
                | otherwise = y0
        sy      | y1 >= y0  = y0
                | otherwise = y1
        calcy x | y0 <= y1  = floor $ fromIntegral dy / fromIntegral dx * fromIntegral (x-x0) + fromIntegral sy + 0.5
                | otherwise = floor $ fromIntegral dy / fromIntegral dx * fromIntegral (x1-x) + fromIntegral sy + 0.5
        calcx y | y0 <= y1  = floor $ fromIntegral (x1-x0) / fromIntegral (by-sy) * fromIntegral (y-sy) + fromIntegral x0 + 0.5
                | otherwise = floor $ fromIntegral (x1-x0) / fromIntegral (by-sy) * fromIntegral (by-y) + fromIntegral x0 + 0.5

-- can Entity see tile at (Int, Int) if it has sight range Int
canSee :: GameMap -> Entity -> Int -> (Int, Int) -> Bool
canSee gm e range (x,y)
  | ex == x && ey == y        = True
  | dx > range || dy > range  = False
  | otherwise                 = all (\(tx,ty) -> tileTransparent $ getGameTile (tx,ty) gm) $ init $ bresenhamPoints (ex,ey) (x,y)
  where ex  = entityXPos e
        ey  = entityYPos e
        dx  = abs (ex - x)
        dy  = abs (ey - y)

-- return gamemap with visible entities
gameMapWithEntities :: GameState -> GameMap
gameMapWithEntities gs = foldr putentity gm es
  where gm          = gameMap gs
        p           = player gs
        es          = filter (\e -> canSee gm p 8 (entityXPos e, entityYPos e)) $ p : gameEntities gm
        putentity e = setGameTile (entityXPos e, entityYPos e) (e2t e)
        e2t e       = newTile { tileChar            = entityChar e
                              , tileVisibleAttrName = entityAttr e
                              }

tileIsSeen :: GameState -> (Int, Int) -> Bool
tileIsSeen gs (x,y) = getGameTileSeen x y gm
  where gm  = gameMap gs

tileIsVisible :: GameState -> (Int, Int) -> Bool
tileIsVisible gs = canSee gm p 8
  where gm  = gameMap gs
        p   = player gs
