module Entity
  ( Entity (..)
  , newEntity
  , moveEntity
  ) where

import Brick.AttrMap (AttrName)

data Entity = Entity
  { entityName  :: String
  , entityXPos  :: Int
  , entityYPos  :: Int
  , entityChar  :: Char
  , entityAttr  :: AttrName
  } deriving (Eq, Show)

newEntity :: String -> Int -> Int -> Char -> AttrName -> Entity
newEntity = Entity

moveEntity :: Entity -> Int -> Int -> Entity
moveEntity entity dx dy = entity  { entityXPos = entityXPos entity + dx
                                  , entityYPos = entityYPos entity + dy
                                  }
