{-# Language OverloadedStrings #-}

module GameMap
  ( GameMap (..)
  , GameTile (..)
  , Coord
  , TileMap
  , newGameMap
  , walledGameMap
  , filledGameMap
  , getGameTile
  , setGameTile
  , setGameTileSeen
  , getGameTileSeen
  , newTile
  , floorTile
  , wallTile
  , emptyFloor
  , entitiesAtPos
  ) where

import Data.Maybe (fromMaybe)
import Brick.AttrMap (AttrName)
import qualified Data.IntMap.Strict as M

import Entity

data GameTile = GameTile
  { tileChar            :: Char
  , tileVisibleAttrName :: AttrName   -- attr for currently seen
  , tileSeenAttrName    :: AttrName   -- attr for previously seen
  , tileWalkable        :: Bool       -- can walk through?
  , tileTransparent     :: Bool       -- can see through?
  } deriving (Eq, Show)

type TileMap = M.IntMap GameTile

data GameMap = GameMap
  { gameTiles     :: TileMap
  , gameSeenTiles :: M.IntMap Bool
  , gameMapWidth  :: Int
  , gameMapHeight :: Int
  , gameEntities  :: [Entity]
  } deriving (Eq, Show)

type Coord = (Int, Int)

newTile :: GameTile
newTile = GameTile
  { tileChar            = ' '
  , tileVisibleAttrName = "blank"
  , tileSeenAttrName    = "blank"
  , tileWalkable        = False
  , tileTransparent     = False
  }

floorTile :: Char -> AttrName -> GameTile
floorTile c attr = GameTile
  { tileChar            = c
  , tileVisibleAttrName = attr
  , tileSeenAttrName    = "oldfloor"
  , tileWalkable        = True
  , tileTransparent     = True
  }

wallTile :: Char -> AttrName -> GameTile
wallTile c attr = GameTile
  { tileChar            = c
  , tileVisibleAttrName = attr
  , tileSeenAttrName    = "oldwall"
  , tileWalkable        = False
  , tileTransparent     = False
  }

-- emptyFloor = floorTile '█' "floor"
emptyFloor = floorTile '.' "floor"

-- new empty gamemap with supplied width and height
newGameMap :: Int -> Int -> GameMap
newGameMap w h = GameMap M.empty M.empty w h []

walledGameMap :: Int -> Int -> GameMap
walledGameMap w h = foldr setwall (newGameMap w h) walls
  where xs            = [0..w-1]
        ys            = [0..h-1]
        walls         = [ (x,0) | x <- xs ] <> [ (x,h-1) | x <- xs ] <> [ (0,y) | y <- ys ] <> [ (w-1,y) | y <- ys ]
        setwall (x,y) = setGameTile (x,y) (wallTile '█' "wall")

-- all walls
filledGameMap :: Int -> Int -> GameMap
filledGameMap w h = foldr setwall (newGameMap w h) [ (x,y) | x <- [0..w-1], y <- [0..h-1] ]
  where setwall (x,y) = setGameTile (x,y) (wallTile '█' "wall")

getGameTile :: Coord -> GameMap -> GameTile
getGameTile (x,y) gm = fromMaybe emptyFloor (M.lookup k m)
  where m   = gameTiles gm
        w   = gameMapWidth gm
        k   = y*w+x

setGameTile :: Coord -> GameTile -> GameMap -> GameMap
setGameTile (x,y) o gm = gm { gameTiles = objs }
  where m     = gameTiles gm
        w     = gameMapWidth gm
        k     = y*w+x
        objs  = M.insert k o m

setGameTileSeen :: Int -> Int -> GameMap -> GameMap
setGameTileSeen x y gm = gm { gameSeenTiles = tiles' }
  where w       = gameMapWidth gm
        k       = y*w+x
        tiles'  = M.insert k True $ gameSeenTiles gm

getGameTileSeen :: Int -> Int -> GameMap -> Bool
getGameTileSeen x y gm = fromMaybe False $ M.lookup k $ gameSeenTiles gm
  where w = gameMapWidth gm
        k = y*w+x

entitiesAtPos :: (Int, Int) -> GameMap -> [Entity]
entitiesAtPos (x,y) gm = filter isAt $ gameEntities gm
  where isAt e  = entityXPos e == x && entityYPos e == y

