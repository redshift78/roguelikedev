module DungeonSettings
  ( DungeonSettings (..)
  , defaultDungeonSettings
  ) where

data DungeonSettings = DungeonSettings
  { dungeonWidth        :: Int
  , dungeonHeight       :: Int
  , dungeonMinRoomWH    :: Int
  , dungeonMaxRoomWH    :: Int
  , dungeonMinRooms     :: Int
  , dungeonMaxRooms     :: Int
  , dungeonMaxMonsters  :: Int    -- maximum number of monsters per room
  }

defaultDungeonSettings :: DungeonSettings
defaultDungeonSettings  = DungeonSettings
  { dungeonWidth        = 80
  , dungeonHeight       = 30
  , dungeonMinRoomWH    = 5
  , dungeonMaxRoomWH    = 14
  , dungeonMinRooms     = 4
  , dungeonMaxRooms     = 10
  , dungeonMaxMonsters  = 2
  }
