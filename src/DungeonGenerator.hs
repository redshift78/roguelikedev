{-# Language OverloadedStrings #-}

module DungeonGenerator
  ( digRoom
  , digTunnel
  , randomDungeon
  ) where

import System.Random

import Entity
import GameMap
import Monsters
import DungeonSettings

data Room = Room
  { roomLeft      :: Int    -- x
  , roomTop       :: Int    -- y
  , roomWidth     :: Int
  , roomHeight    :: Int
  , roomMonsters  :: [Entity]
  } deriving (Eq, Show)

roomCenterX :: Room -> Int
roomCenterX room = roomLeft room + (roomWidth room `div` 2)

roomCenterY :: Room -> Int
roomCenterY room = roomTop room + (roomHeight room `div` 2)

roomRight :: Room -> Int
roomRight room = roomLeft room + roomWidth room - 1

roomBottom :: Room -> Int
roomBottom room = roomTop room + roomHeight room - 1

roomsOverlap :: Room -> Room -> Bool
roomsOverlap r1 r2  = r1x1 <= r2x2 && r1x2 >= r2x1 && r1y1 <= r2y2 && r1y2 >= r2y1
  where r1x1  = roomLeft r1
        r1y1  = roomTop r1
        r1x2  = roomRight r1
        r1y2  = roomBottom r1
        r2x1  = roomLeft r2
        r2y1  = roomTop r2
        r2x2  = roomRight r2
        r2y2  = roomBottom r2

validRoom :: DungeonSettings -> Room -> Bool
validRoom settings room
  | roomLeft room < 0                           = False
  | roomTop room < 0                            = False
  | roomRight room > dungeonWidth settings-1    = False
  | roomBottom room > dungeonHeight settings-1  = False
  | roomWidth room < dungeonMinRoomWH settings  = False
  | roomWidth room > dungeonMaxRoomWH settings  = False
  | roomHeight room < dungeonMinRoomWH settings = False
  | roomHeight room > dungeonMaxRoomWH settings = False
  | otherwise                                   = True

randomMonster :: RandomGen g => (Int, Int) -> (Int, Int) -> g -> (Entity, g)
randomMonster (x1, y1) (x2, y2) g = (monster x y, g3)
  where (rand, g1)  = randomR (0, 100) g
        monster     = if (rand::Int) <= 80 then newOrc else newTroll
        (x, g2)     = randomR (x1, x2) g1
        (y, g3)     = randomR (y1, y2) g2

randomMonsters' :: RandomGen g => (Int, Int) -> (Int, Int) -> Int -> [Entity] -> g -> ([Entity], g)
randomMonsters' tl br n es g
  | length es == n  = (es, g)
  | otherwise       = randomMonsters' tl br n (m:es) g'
  where (m, g') = randomMonster tl br g

randomMonsters :: RandomGen g => (Int, Int) -> (Int, Int) -> Int -> g -> ([Entity], g)
randomMonsters tl br n = randomMonsters' tl br n []

randomRoom :: RandomGen g => DungeonSettings -> g -> (Room, g)
randomRoom settings g
  | validRoom settings room = (room, g4)
  | otherwise               = randomRoom settings g6
  where room  = Room  { roomLeft      = left
                      , roomTop       = top
                      , roomWidth     = width
                      , roomHeight    = height
                      , roomMonsters  = mons
                      }
        (left, g1)    = randomR (0, dungeonWidth settings) g
        (top, g2)     = randomR (0, dungeonHeight settings) g1
        (width, g3)   = randomR (dungeonMinRoomWH settings, dungeonMaxRoomWH settings) g2
        (height, g4)  = randomR (dungeonMinRoomWH settings, dungeonMaxRoomWH settings) g3
        (nummons, g5) = randomR (0, dungeonMaxMonsters settings) g4
        (mons, g6)    = randomMonsters (left+2,top+2) (left+width-2,top+height-2) nummons g5

randomRooms' :: RandomGen g => DungeonSettings -> Int -> g -> [Room] -> ([Room], g)
randomRooms' settings n g rooms
  | length rooms == n = (rooms, g)
  | otherwise         = randomRooms' settings n g' (room : rooms)
  where (room, g')    = randomRoom settings g

randomRooms :: RandomGen g => DungeonSettings -> Int -> g -> ([Room], g)
randomRooms settings n g = randomRooms' settings n g []

randomDungeon :: RandomGen g => DungeonSettings -> g -> (GameMap, g)
randomDungeon settings g = (gm', g2)
  where (n, g1)         = randomR (dungeonMinRooms settings, dungeonMaxRooms settings) g
        (rooms, g2)     = randomRooms settings n g1
        tunnelpairs     = [ (rooms!!i, rooms!!(i+1)) | i <- [0..length rooms-2] ]
        w               = dungeonWidth settings
        h               = dungeonHeight settings
        tunnel (r1, r2) = digTunnel (roomCenterX r1) (roomCenterY r1) (roomCenterX r2) (roomCenterY r2)
        roomed          = foldr digRoom (filledGameMap w h) rooms
        tunnelled       = foldr tunnel roomed tunnelpairs
        mons            = concatMap roomMonsters rooms
        gm'             = tunnelled { gameEntities = mons }

-- dig a rectangular room
-- the starting and ending coordinates are walls
digRoom :: Room -> GameMap -> GameMap
digRoom room gm  = foldr setfloor gm [ (x,y) | x <- [x1+1..x2-1], y <- [y1+1..y2-1] ]
  where setfloor (x,y)  = setGameTile (x,y) emptyFloor
        x1              = roomLeft room
        x2              = roomRight room
        y1              = roomTop room
        y2              = roomBottom room

-- dig a tunnel from coordinate to coordinate
-- always digs horisontally first, then vertically
-- might change to more of a 'straight' line alg
digTunnel :: Int -> Int -> Int -> Int -> GameMap -> GameMap
digTunnel x1 y1 x2 y2 gm
  | x1 > x2   = digTunnel x2 y2 x1 y1 gm        -- go from left to right
  | x1 < x2   = digTunnel (x1+1) y1 x2 y2 gm'
  | y1 > y2   = digTunnel x2 y2 x1 y1 gm        -- go from top to bottom
  | y1 < y2   = digTunnel x1 (y1+1) x2 y2 gm'
  | otherwise = gm'
  where gm'   = setGameTile (x1,y1) emptyFloor gm
