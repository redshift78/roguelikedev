{-# Language OverloadedStrings #-}

module GameUI
  ( UIState (..)
  , defaultUIState
  , newUIStateWithMap
  , newUIStateWithGameState
  , app
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on)
import Brick.Widgets.Core

import qualified Graphics.Vty as V
import qualified Brick.AttrMap as A

import Entity
import GameMap
import GameState

data UIEvent = StepEvent deriving (Eq, Show)
type UINames = ()

newtype UIState = UIState
  { gameState   :: GameState
  } deriving (Eq, Show)

defaultUIState :: UIState
defaultUIState = UIState
  { gameState = defaultGameState
  }

newUIStateWithMap :: GameMap -> UIState
newUIStateWithMap gm = defaultUIState { gameState = newGameStateWithMap gm }

newUIStateWithGameState :: GameState -> UIState
newUIStateWithGameState gs = defaultUIState { gameState = gs }

app :: App UIState UIEvent UINames
app = App { appDraw         = drawUI
          , appChooseCursor = \_ _ -> Nothing
          , appHandleEvent  = eventHandler
          , appStartEvent   = return
          , appAttrMap      = attrMap
          }

drawMap :: UIState -> Widget UINames
drawMap state = vBox $ map row [0..(h-1)]
  where gs    = gameState state
        p     = player gs
        m     = gameMapWithEntities gs
        w     = gameMapWidth m
        h     = gameMapHeight m
        char xy | tileIsVisible gs xy = tileChar (getGameTile xy m)
                | tileIsSeen gs xy    = tileChar (getGameTile xy m)
                | otherwise           = ' '
        attr xy | tileIsVisible gs xy = tileVisibleAttrName (getGameTile xy m)
                | tileIsSeen gs xy    = tileSeenAttrName (getGameTile xy m)
                | otherwise           = "unseen"
        t2w xy  = withAttr (attr xy) $ str [char xy]
        row y = hBox [ t2w (x,y) | x <- [0..(w-1)]]

drawTextArea :: UIState -> Widget UINames
drawTextArea state  = vBox $ map str ts
  where ts  = reverse $ take 5 $ gameText $ gameState state

drawUI :: UIState -> [ Widget UINames ]
drawUI state = [ drawMap state <=> drawTextArea state ]
  where gs      = gameState state
        px      = entityXPos $ player gs
        py      = entityYPos $ player gs
        pc      = entityChar $ player gs
        vspace  = vBox $ replicate py (str " ")
        hspace  = hBox $ replicate px $ str " "

eventHandler :: UIState -> BrickEvent UINames UIEvent -> EventM UINames (Next UIState)
eventHandler state (VtyEvent e) =
  case e of
    V.EvKey V.KEsc []         -> halt state
    V.EvKey (V.KChar 'q') []  -> halt state
    V.EvKey (V.KChar 'h') []  -> continue $ state { gameState = performAction DirectionLeft gs }
    V.EvKey (V.KChar 'l') []  -> continue $ state { gameState = performAction DirectionRight gs }
    V.EvKey (V.KChar 'j') []  -> continue $ state { gameState = performAction DirectionDown gs }
    V.EvKey (V.KChar 'k') []  -> continue $ state { gameState = performAction DirectionUp gs }
    V.EvKey (V.KChar 'y') []  -> continue $ state { gameState = performAction DirectionUpLeft gs }
    V.EvKey (V.KChar 'b') []  -> continue $ state { gameState = performAction DirectionDownLeft gs }
    V.EvKey (V.KChar 'u') []  -> continue $ state { gameState = performAction DirectionUpRight gs }
    V.EvKey (V.KChar 'n') []  -> continue $ state { gameState = performAction DirectionDownRight gs }
    _                         -> continue state
  where gs  = gameState state

eventHandler state _  = continue state

myWhite = V.rgbColor 255 255 255
myGrey  = V.rgbColor 10 10 10

attrMap :: UIState -> A.AttrMap
attrMap state = A.attrMap V.defAttr [ ("player",    V.brightWhite   `on` V.black)
                                    , ("npc",       V.brightYellow  `on` V.black)
                                    , ("monster",   V.brightYellow  `on` V.black)
                                    , ("floor",     myWhite         `on` V.black)
                                    , ("oldfloor",  myGrey          `on` V.black)
                                    , ("wall",      myWhite         `on` V.black)
                                    , ("oldwall",   myGrey          `on` V.black)
                                    , ("seeing",    V.brightYellow  `on` V.black)
                                    , ("unseen",    V.black         `on` V.black)
                                    , ("debug",     V.brightRed     `on` V.black)
                                    ]
