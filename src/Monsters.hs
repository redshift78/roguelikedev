{-# Language OverloadedStrings #-}

module Monsters
  ( newTroll
  , newOrc
  ) where

import Entity

newTroll x y  = newEntity "troll" x y 'T' "monster"
newOrc x y    = newEntity "orc"   x y 'o' "monster"
